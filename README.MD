# TASK MANAGER

## DEVELOPER INFO

**NAME:** Evgeny Tsepkov

**EMAIL:** markmilson@yandex.ru

**EMAIL:** eatsepkov@t1-consulting.ru

## SOFTWARE

**JAVA:** HotSpot 1.8

**OS:** MACOS BIG SUR 11.6

## HARDWARE

**CPU:** APPLE M1

**RAM:** 16GB

**SSD:** 1TB

## BUILD PROGRAM

```
mvn clean install
```

## RUN PROGRAM

```
java -jar ./task-manager.jar
```
