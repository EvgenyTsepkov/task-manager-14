package ru.tsc.tsepkov.tm.api;

import ru.tsc.tsepkov.tm.model.Task;

public interface ITaskController {

    void createTask();

    void showTasks();

    void showTaskById();

    void showTask(Task task);

    void showTaskByProjectId();

    void showTaskByIndex();

    void clearTasks();

    void removeTaskById();

    void removeTaskByIndex();

    void updateTaskById();

    void updateTaskByIndex();

    void startTaskById();

    void startTaskByIndex();

    void completeTaskById();

    void completeTaskByIndex();

    void changeTaskStatusById();

    void changeTaskStatusByIndex();

}
