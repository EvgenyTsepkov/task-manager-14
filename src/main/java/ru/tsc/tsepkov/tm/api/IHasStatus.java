package ru.tsc.tsepkov.tm.api;

import ru.tsc.tsepkov.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);

}
