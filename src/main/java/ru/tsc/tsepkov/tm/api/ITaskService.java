package ru.tsc.tsepkov.tm.api;

import ru.tsc.tsepkov.tm.enumerated.Sort;
import ru.tsc.tsepkov.tm.enumerated.Status;
import ru.tsc.tsepkov.tm.model.Task;

import java.util.List;

public interface ITaskService extends ITaskRepository{

    Task create(String name, String description);

    List<Task> findAll(Sort sort);

    Task updateById(String id, String name, String description);

    Task updateByIndex(Integer index, String name, String description);

    Task changeTaskStatusById(String id, Status status);

    Task changeTaskStatusByIndex(Integer index, Status status);

}
